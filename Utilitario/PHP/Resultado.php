<?php
class Resultado{
// --------------------------------------------------------------------------------------------------------------

	const	ERROR_CLAVE_FORANEA_VIOLADA			= 1;
	const	ERROR_CLAVE_PRIMARIA_VIOLADA		= 2;
	const	ERROR_UNICIDAD_VIOLADA				= 3;
	const	ERROR_GENERAL						= 4;
	const	ERROR_PROBLEMAS_CONEXION			= 5;
	
	const RESULTADO_EXITOSO		= TRUE;
	const RESULTADO_SIN_EXITO 	= FALSE;	
	
	private $codigoError;
	
	private $resultadoExitosoSiNo;
	
	private $mensaje;
	
	private $recurso;
	
// --------------------------------------------------------------------------------------------------------------

	
	public function __construct(){
		$codigoError				= -1;
		$this->resultadoExitosoSiNo = TRUE;
	}
	
	public function getCodigoError(){
		return( $this->codigoError );
	}
	
	public function setCodigoError( $pCodigoError ){
		$this->codigoError = $pCodigoError;
	}
	
	public function getEstado(){
		return( $this->resultadoExitosoSiNo );
	}
	
	public function getMensaje(){
		return( $this->mensaje );
	}
	
	public function getRecurso(){
		return( $this->recurso );
	}
	
	public function setEstado( $pResultadoExitosoSiNo ){
		$this->resultadoExitosoSiNo = $pResultadoExitosoSiNo;
	}
	
	public function setMensaje( $pMensaje ){                
		$this->mensaje = $pMensaje;	                
	}
	
	public function setRecurso( $pRecurso ){
		$this->recurso = $pRecurso;				
	}		
}
 ?>
