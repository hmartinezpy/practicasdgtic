<?php
class JsonConsumidor implements Consumidor{
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
    
    /** Constructores *********************************************************/
    public function __construct(){
    }
    /** Constructores *********************************************************/
	
    private function obtenerCamelCase( $cadena ){
    // -------------------------------------------------------------------------
        $index; $cantidadDatos;
        $camelCase;
        $vectorPartes;        
    // -------------------------------------------------------------------------

        $vectorPartes   = explode( "_", $cadena );
        
        $camelCase      = $vectorPartes[ 0 ];
        $cantidadDatos  = count( $vectorPartes );
        
        for( $indice=1; $indice < $cantidadDatos; $indice++ ){
            $camelCase.= strtoupper( substr ( $vectorPartes[ $indice ] , 0, 1 ) ).substr ( $vectorPartes[ $indice ] ,1);
        }            
        
        return( $camelCase );
    }
    
    private function obtenerJson( $vector ){
    // -------------------------------------------------------------------------
        $vectorIndices;
        $json;
    // -------------------------------------------------------------------------
        
        $vectorIndices  = array_keys( $vector );
        
        $json           = "";
        
        foreach( $vectorIndices AS $nombreIndice ){
            if( !is_numeric( $nombreIndice ) )
                $json.= ',"'.$this->obtenerCamelCase( $nombreIndice ).'":"'.$vector[ $nombreIndice ].'"';
        }
        
        if( $json !== "" ){
            $json = substr( $json, 1 );
        }
        
        return( "$json");
        
    }
    
    public function consumirDatos( $objetoResultado, $manejadorSgbd ){
    // -------------------------------------------------------------------------
        $objetoResultado;
        $json;
    // -------------------------------------------------------------------------       
        
        $filasBaseDatos     = $objetoResultado->getRecurso();
        
        $objetoResultado    = new Resultado();
        
        $json   = '';

        while( $fila = $manejadorSgbd->obtenerFila( $filasBaseDatos ) ){
            $json.= ',{'.$this->obtenerJson( $fila ).'}';
        }
        
        if( $json != '' ){
            $json = substr($json, 1 );
            $json = '{"resultado_exitoso":true,"datos":['.$json.']}';
        }else{
            $json= '{"resultado_exitoso":false,"datos":null}';
        }
        
        $objetoResultado->setRecurso( $json );

        return( $objetoResultado );
    }
}
?>