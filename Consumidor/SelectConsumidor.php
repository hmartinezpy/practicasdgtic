<?php
class SelectConsumidor implements Consumidor{

    public function __construct(){}

    public function consumirDatos( $objetoResultado, $manejadorSgbd ){
    // -------------------------------------------------------------------------
        $clase;
        $contador;
        $objetoResultado;
        $fila;
        $html;
        $filasBaseDatos;
    // -------------------------------------------------------------------------

        $filasBaseDatos = $objetoResultado->getRecurso();

        $html=	'';                      

        while( $fila = $manejadorSgbd->obtenerFila( $filasBaseDatos ) ){                      

            $html.= "<option value='".$fila[ 'codigo' ]."'>".$fila[ 'etiqueta' ]."</option>";            
        }

        if( $html == "" )
            $html= "<option value=''>No se encontraron datos</option>";

        
        $objetoResultado->setRecurso( $html );

        return( $objetoResultado );
    }
}
?>