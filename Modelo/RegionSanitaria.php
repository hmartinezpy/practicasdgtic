<?php
class RegionSanitaria extends Modelo
{
    public function __construct()
    {
        
    }
    
    public function obtenerRegionesSanitarias( $consumidor )
    {// ------------------------------------------------------------------------
        $sql;
    // -------------------------------------------------------------------------
        
        $sql = 'SELECT codigo, nombre AS etiqueta'
                .' FROM region_sanitaria '
                .' ORDER BY etiqueta;';
        
        return( $this->ejecutarConsultaConsumible( $sql , $consumidor ) );
    }
}
?>