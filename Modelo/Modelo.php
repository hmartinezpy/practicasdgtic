<?php
class Modelo{
// -----------------------------------------------------------------------------    
    protected  	$manejadorSgbd;
// -----------------------------------------------------------------------------
	
    public function __construct(){}
    
    public function getManejadorSgbd(){
        return( $this->manejadorSgbd );
    }

    public function setManejadorSgbd( $pManejadorSgbd ){
        $this->manejadorSgbd= $pManejadorSgbd;
    }

    protected function ejecutarConsultaConsumible( $sql, $consumidor ){
    // -------------------------------------------------------------------------
        $objetoResultado;
    // -------------------------------------------------------------------------

        $objetoResultado = $this->manejadorSgbd->ejecutarSql( $sql );

        if( $objetoResultado->getEstado() != Resultado::RESULTADO_SIN_EXITO )
            $objetoResultado = $consumidor->consumirDatos( $objetoResultado, $this->manejadorSgbd );

        return( $objetoResultado );
    }   
}
?>