<?php        
   
    $conexion   = pg_connect ( 'host=localhost port=5432 dbname=herencia user=postgres password=' ) 
                  OR die( 'FALLO AL INTENTAR CONEXIÓN' );
    
    $sql        = "BEGIN";
        
    $resultado  = pg_query ( $conexion, $sql );
    
            
    if( $resultado ){
        echo( "INICIA TRANSACCION <br/>" );
    }else{
        echo( "SE ENCONTRARON INCONVENIENTES AL INTERACTUAR CON LA BASE DE DATOS <br/>" );
    }
   
    $sql        = "INSERT INTO persona( codigo, nombres, apellidos )VALUES
                ( 2, 'JUAN', 'PEREZ' );";
    
    $resultado  = pg_query ( $conexion, $sql );
    
            
    if( $resultado ){
        echo( "LOS DATOS HAN SIDO REGISTRADOS CON ÉXITO <br/>" );
    }else{
        echo( "SE ENCONTRARON INCONVENIENTES AL INTERACTUAR CON LA BASE DE DATOS <br/>" );
    }
    
    $sql        = "INSERT INTO usuario( codigo_persona, identificador, contrasenia )VALUES
                    ( 2, 'jperez', '#%/*#%/*#%/*' );";
    
    
    $resultado  = pg_query ( $conexion, $sql );
    
            
    if( $resultado ){
        
        $sql        = "END;";
        
        $resultado  = pg_query ( $conexion, $sql );
        
        
        echo( "LOS DATOS HAN SIDO REGISTRADOS CON ÉXITO <br/>" );
        
    }else{
        
        $sql        = "ROLLBACK;";
        
        $resultado  = pg_query ( $conexion, $sql );
        
        echo( "SE ENCONTRARON INCONVENIENTES AL INTERACTUAR CON LA BASE DE DATOS <br/>" );
    }                            
    
    pg_close( $conexion );
?>