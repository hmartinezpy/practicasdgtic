<?php
class Establecimiento{
    // Atributos   
    private $nombreEstablecimiento;
    private $codigoRegionSanitaria;
    
    public function __construct() {
        
    }
    
    // Métodos
    public function getNombreEstablecimiento() {
        return $this->nombreEstablecimiento;
    }

    public function getCodigoRegionSanitaria() {
        return $this->codigoRegionSanitaria;
    }

    public function setNombreEstablecimiento($nombreEstablecimiento) {
        $this->nombreEstablecimiento = $nombreEstablecimiento;
    }

    public function setCodigoRegionSanitaria($codigoRegionSanitaria) {
        $this->codigoRegionSanitaria = $codigoRegionSanitaria;
    }


    public function guardarEstablecimiento(){
        
        //echo( "Establecimiento guardado" );
        
        // 0) Armar SQL
        // 1) Obtener conexión
        // 2) Ejecutar SQL
        // 3) Retornar resultado                        
        
        /**/
        $sql = 'INSERT INTO establecimiento( .... );';                       
        
        
        $manejadorSgbd  = FabricaAbstractaSgbd
                            ::obtenerSgbd( FabricaAbstractaSgbd::SGBD_POR_DEFECTO );
        
        $conexion = $manejadorSgbd->obtenerConexion();
        
        $manejadorSgbd->ejecutarSql( $sql );
        
        $manejadorSgbd->cerrarConexion();
        /**/
        
        // ....
        
        /**
        // 0
        $sql = 'INSERT INTO establecimiento( .... );';
                
        // 1
        $conexion = pg_conect( '....' );
        
        // 2 
        $resultado = pg_query( $sql );
        
        // ......
        
        pg_close( $conexion );
         
         /**/
        
    }
}
?>

