<?php
class TipoEstablecimiento extends Modelo
{
    public function __construct()
    {
        
    }
    
    public function obtenerTiposEstablecimientos( $consumidor )
    {// ------------------------------------------------------------------------
        $sql;
    // -------------------------------------------------------------------------
        
        $sql = "SELECT codigo, nombre || ' ' || '(' || abreviatura || ')' AS etiqueta"
                .' FROM tipo_establecimiento'
                .' ORDER BY etiqueta;';
        
        return( $this->ejecutarConsultaConsumible( $sql , $consumidor ) );
    }
}
?>