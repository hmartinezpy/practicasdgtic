<?php
class Distrito extends Modelo
{
    public function __construct()
    {
        
    }
    
    public function obtenerTiposEstablecimientos( $consumidor )
    {// ------------------------------------------------------------------------
        $sql;
    // -------------------------------------------------------------------------
        
        $sql = 'SELECT codigo, nombre AS etiqueta'
                .' FROM distrito'
                .' ORDER BY etiqueta;';
        
        return( $this->ejecutarConsultaConsumible( $sql , $consumidor ) );
    }
}
?>