<?php    
//include '../../Vista/Establecimiento/EstablecimientoNuevo.php';
/**/
include '../../Utilitario/PHP/Resultado.php';
include '../../Persistencia/Configuracion.php';
include '../../Persistencia/ManejadorSgbd.php';
include '../../Persistencia/ManejadorSgbdPostgres/ManejadorSgbdPostgreSql.php';
include '../../Persistencia/FabricaAbstractaSgbd.php';

include '../../Modelo/Modelo.php';
include '../../Modelo/TipoEstablecimiento.php';
include '../../Modelo/RegionSanitaria.php';

include '../../Consumidor/Consumidor.php';
include '../../Consumidor/SelectConsumidor.php';

session_start();
prepararFormularioEstablecimientoNuevo();

function prepararFormularioEstablecimientoNuevo(){
    
        
    $manejadorSgbd = FabricaAbstractaSgbd::obtenerSgbd( FabricaAbstractaSgbd::SGBD_POR_DEFECTO );
    $consumidor = new SelectConsumidor();
    
    
    // Tipos de establecimiento
    $modelo = new TipoEstablecimiento();
    $modelo->setManejadorSgbd( $manejadorSgbd  );        
            
    $resultado  = $modelo->obtenerTiposEstablecimientos( $consumidor );
    
    $_SESSION[ 'tiposEstablecimientos' ]= $resultado->getRecurso();
    
    
    // Regiones sanitarias
    
    $modelo = new RegionSanitaria();
    $modelo->setManejadorSgbd( $manejadorSgbd );        
    
    $resultado  = $modelo->obtenerRegionesSanitarias( $consumidor );
    
    $_SESSION[ 'regionesSanitarias' ]= $resultado->getRecurso();
    
    header( 'location: ../../Vista/Establecimiento/EstablecimientoNuevo.php' );
    //include '../../Vista/Establecimiento/EstablecimientoNuevo.php';
}
/**/  
?>

