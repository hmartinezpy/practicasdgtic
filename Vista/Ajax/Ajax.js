function Ajax_obtenerObjetoAjax(){
    
    return( new XMLHttpRequest() );
}

function Ajax_controlarEstado(){
    
}

function Ajax_enviarSolicitud( url ){
// -----------------------------------------------------------------------------
    var objetoAjax;
// -----------------------------------------------------------------------------

    objetoAjax  = Ajax_obtenerObjetoAjax();

    objetoAjax.open( 'POST', url, true );
    
    objetoAjax.onreadystatechange   = function () {
        
        if( objetoAjax.readyState === XMLHttpRequest.DONE && objetoAjax.status === 200) {
            alert( objetoAjax.responseText );
        }
    };
    
    objetoAjax.send();    
}