<?php
?>
<html>
    <head>
        <link rel="stylesheet" href="CSS/BootStrap/css/bootstrap.min.css"/>
        
        <style>
            .Menu_botonPrincipal{
                color:white;
                background-color:#1E90FF;
                border:solid 0.5px white;
                height:2em;
                cursor:pointer;
            }
            
            #Menu_encabezado{
                border-bottom:solid 1px blue;
            }
            
        </style>
        
        <script type="text/javascript">
            function Menu_opcion01(){
                alert( "Menu 01" );
            }
            
            function Establecimiento_guardar(){
            // ---------------------------------------------------------------
                var elementoHTML;
            // ---------------------------------------------------------------
            
                elementoHTML = document.getElementById( "nombreEstablecimiento" );
                
                if( elementoHTML.value.trim() == "" ){
                    alert( "El nombre del establecimiento es obligatorio" );
                    return;
                }
                
                alert( "Guardando datos..." );
            }
        </script>
    </head>
    <body>
        <div id="Menu_encabezado">
            <table>
                <tr>
                    <td>
                        <input type="button"  class="Menu_botonPrincipal" value="Inicio" onClick="Menu_opcion01()"/>
                    </td>
                    <td>
                        <input type="button"  class="Menu_botonPrincipal" value="Menu 01" onClick="Menu_opcion01()" />
                    </td>
                    <td>
                        <input type="button"  class="Menu_botonPrincipal" value="Menu 02" onClick="Menu_opcion01()" />
                    </td>
                    <td>
                        <input type="button"  class="Menu_botonPrincipal" value="Menu 03" onClick="Menu_opcion01()" />
                    </td>
                </tr>
            </table>            
        </div>
        <div id="contenedorDatos" style="margin-left:10%;width:80%;margin-top:10em;border:solid 1px green">
            <form method="POST" action="../../Controlador/Establecimiento/EstablecimientoRegistroControlador.php">
            <table>
                <tr><td>Nombre</td>
                    <td><input type="text" id="nombreEstablecimiento" name="nombreEstablecimiento"/></td>
                </tr>           
                <tr><td>Tipo establecimiento</td>
                    <td><select id="codigoTipoEstablecimiento" name="codigoTipoEstablecimiento">                        
                            <?php echo( $_SESSION[ 'tiposEstablecimientos' ] );?>
                        </select></td>
                </tr>            
                <tr><td>Región</td>
                    <td><select id="codigoRegionSanitaria" name="codigoRegionSanitaria">
                            <?php echo( $_SESSION[ 'regionesSanitarias' ] );?>
                        </select></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="button" value="guardar" onClick="Establecimiento_guardar()"/>
                    </td>
                </tr>
            </table>     
            </form>
        </div>        
    </body>
</html>