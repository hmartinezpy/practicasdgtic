<?php
class ManejadorSgbdPostgreSql implements ManejadorSgbd{
// -----------------------------------------------------------------------------
    private $configuracion;
    private $conexion;
    private $resultado;
// -----------------------------------------------------------------------------

    public function __construct( $pConfiguracion ){
        $this->configuracion = $pConfiguracion;
        $this->resultado    = NULL;
        $this->conexion     = NULL;
    }

    public function abrirConexion(){
        $this->conexion= $this->obtenerConexion();
    }

    public function analizarBooleano( $datoBooleanoSql ){

        if( $datoBooleanoSql == 't' )
            return( TRUE );
        else
            return( FALSE );
    }

    public function obtenerConexion(){
    // -------------------------------------------------------------------------
        $conexionAuxiliar;
        $resultado;
        $objetoResultado;
    // -------------------------------------------------------------------------

        $objetoResultado = new Resultado();

        if( $this->conexion == NULL ){
            $conexionAuxiliar= @pg_connect( 
                            "host=".$this->configuracion->getServidorBaseDatos()."
                            port=".$this->configuracion->getPuerto()."
                            password=".$this->configuracion->getContraseniaUsuarioBaseDatos()."
                            user=".$this->configuracion->getUsuarioBaseDatos()."
                            dbname=".$this->configuracion->getNombreBaseDatos(), PGSQL_CONNECT_FORCE_NEW
                            );

            if( $conexionAuxiliar ){
                $this->conexion = $conexionAuxiliar;
            }else{
                $objetoResultado->setEstado( Resultado::RESULTADO_SIN_EXITO );

                $objetoResultado->setMensaje( "No se pudo conectar a la base de datos" );
            }
        }

        $objetoResultado->setRecurso( $this->conexion );

        return( $objetoResultado );
    }

    public function ejecutarSql( $sql ){
    // -------------------------------------------------------------------------
        $objetoResultado;
    // -------------------------------------------------------------------------

        $objetoResultado = $this->obtenerConexion();

        if( $objetoResultado->getEstado() == Resultado::RESULTADO_EXITOSO ){

            $resultado= @pg_exec( $sql );

            $this->resultado = $resultado;

            if( !$resultado ){
                $objetoResultado->setEstado( Resultado::RESULTADO_SIN_EXITO );

                //echo( pg_ErrorMessage( $objetoResultado->getRecurso() )."<br/><br/>" );
                if( strpos( pg_ErrorMessage( $objetoResultado->getRecurso() ), "violates unique constraint"  )  ){
                    $objetoResultado->setMensaje( "El dato ya existe" );
                    $objetoResultado->setCodigoError( Resultado::ERROR_UNICIDAD_VIOLADA );
                }else if( strpos( pg_ErrorMessage( $objetoResultado->getRecurso() ), "violates foreign key constraint"  )  ||
                    strpos( pg_ErrorMessage( $objetoResultado->getRecurso() ), "viola la llave for"  ) ){
                    $objetoResultado->setMensaje( "Violación de clave foránea. El dato es referenciado por otros elementos en la base de datos" );
                    $objetoResultado->setCodigoError( Resultado::ERROR_CLAVE_FORANEA_VIOLADA );
                }else if( strpos( pg_ErrorMessage( $objetoResultado->getRecurso() ), "unicidad"  )  ){
                    $objetoResultado->setMensaje( "El dato ya existe" );
                    $objetoResultado->setCodigoError( Resultado::ERROR_UNICIDAD_VIOLADA );
                }else{
                    $objetoResultado->setMensaje( "Problemas al intentar operar con la base de datos: <br/><br/> <div style='padding:5 5 5 5;border:dashed 1px black'>".pg_ErrorMessage( $objetoResultado->getRecurso() )."<div>" );
                    $objetoResultado->setCodigoError( Resultado::ERROR_GENERAL );
                }

                $objetoResultado->setMensaje( $objetoResultado->getMensaje()." <div style='display:none'>$sql</div>" );
            }else
                $objetoResultado->setRecurso( $resultado );
        }

        return( $objetoResultado );
    }

    public function cerrarConexion(){
        if( $this->resultado != NULL )
            pg_FreeResult( $this->resultado );

        if( $this->conexion != NULL ){
            if( pg_close( $this->conexion ) )
                $this->conexion = NULL;
            else 
                echo( "LA CONEXION SIGUE ABIERTA" );
        }
    }

    public function obtenerFila( $conjuntoFilasBaseDatos ){
        return( pg_fetch_array( $conjuntoFilasBaseDatos ) );
    }

    public function obtenerIdentificadorUltimaFilaInsertada(){
    // -------------------------------------------------------------------------
        $objetoResultado;
        $identificadorUltimaFilaInsertada;
    // -------------------------------------------------------------------------

        $objetoResultado = new Resultado();
        
        return( $objetoResultado );
    }
}
?>