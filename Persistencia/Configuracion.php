<?php
class Configuracion{   
    private $servidorBaseDatos;
    private $puerto;
    private $nombreBaseDatos;
    private $usuarioBaseDatos;
    private $contraseniaUsuarioBaseDatos;

    public function __construct(){
                
        $this->servidorBaseDatos            = 'localhost';
        $this->puerto                       = '5432';
        $this->nombreBaseDatos              = 'prueba';
        $this->usuarioBaseDatos             = 'postgres';
        $this->contraseniaUsuarioBaseDatos  = 'postgres';        
    }
   
    public function getServidorBaseDatos(){
        return( $this->servidorBaseDatos );
    }

    public function setServidorBaseDatos( $pServidorBaseDatos ){
        $this->servidorBaseDatos = $pServidorBaseDatos;
    }

    public function getNombreBaseDatos(){
        return( $this->nombreBaseDatos );
    }

    public function setNombreBaseDatos( $pNombreBaseDatos ){
        $this->nombreBaseDatos = $pNombreBaseDatos;
    }

    public function getPuerto(){
        return( $this->puerto );
    }

    public function setPuerto( $pPuerto ){
        $this->puerto = $pPuerto;
    }

    public function getUsuarioBaseDatos(){
        return( $this->usuarioBaseDatos );
    }

    public function setUsuarioBaseDatos( $pUsuarioBaseDatos ){
        $this->usuarioBaseDatos = $pUsuarioBaseDatos;
    }

    public function getContraseniaUsuarioBaseDatos(){
        return( $this->contraseniaUsuarioBaseDatos );
    }

    public function setContraseniaUsuarioBaseDatos( $pContraseniaUsuarioBaseDatos ){
        $this->contraseniaUsuarioBaseDatos = $pContraseniaUsuarioBaseDatos;
    } 
}
?>