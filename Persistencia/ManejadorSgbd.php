<?php
interface ManejadorSgbd{
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
    
    public function analizarBooleano( $datoBooleanoSql );

    public function obtenerConexion();

    public function ejecutarSql( $sql );

    public function cerrarConexion();

    public function obtenerFila( $conjuntoFilasBaseDatos );

    public function obtenerIdentificadorUltimaFilaInsertada();
}?>
