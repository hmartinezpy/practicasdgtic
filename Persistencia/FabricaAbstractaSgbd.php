<?php
class FabricaAbstractaSgbd{
    
    const SGBD_POR_DEFECTO  = 0;
    const SGBD_POSTGRES     = 1;
    const SGBD_MYSQL        = 2;
    
    
    public static function obtenerSgbd( $sgbd ){
        $configuracion = new Configuracion();
        
        if( $sgbd == FabricaAbstractaSgbd::SGBD_POSTGRES ){
            return( new ManejadorSgbdPostgres( $configuracion ) );
            
        }else if( $sgbd == FabricaAbstractaSgbd::SGBD_MYSQL ){
            return( new ManejadorSgbdMysql( $configuracion ) );
            
        }else {
            return( new ManejadorSgbdPostgreSql( $configuracion ) );
        }
        
        
    }
}
?>