﻿CREATE OR REPLACE FUNCTION guardar_usuario(	p_nombres_persona		TEXT
						, p_apellidos_persona		TEXT
						, p_identificador_usuario	TEXT
						, p_contrasenia_usuario		TEXT )
RETURNS INT AS $$
DECLARE
	v_codigo_persona	INT;
BEGIN
	--- CUERPO DE LA FUNCIÓN
	INSERT INTO persona( nombres, apellidos )VALUES
	( p_nombres_persona, p_apellidos_persona )
	RETURNING codigo INTO v_codigo_persona;

	INSERT INTO usuario( codigo_persona, identificador, contrasenia )VALUES
	( v_codigo_persona, p_identificador_usuario, p_contrasenia_usuario );

	RETURN( v_codigo_persona );	
END;
$$ LANGUAGE plpgsql;